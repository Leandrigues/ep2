export default {
  setEmail(state, email) {
    state.email = email;
  },
  setUsername(state, username) {
    state.username = username;
  },
  setAccessToken(state, token) {
    state.token = token;
  }
};
