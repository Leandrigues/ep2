const { colors: defaultColors } = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        mypurple: {
          light: "#fefcff",
          DEFAULT: "#f4ebff",
          dark: "#5b0085"
        }
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
